#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

std::map<std::string, std::map<std::string, std::string>> lang = {
    {"en", {
        {"menu", "coronatest v1.0 by Grzesiek11\n\
1000% accurate COVID19 test.\n\
[1] Continue\n\
[2] About\n\
[3] Language\n\
[0] Exit\n"},
        {"about", "1000% accurate, minimalistic and opensource Coronavirus test\n\
GitLab: https://gitlab.com/grzesiek11/coronatest \n\
Heavily inspired by https://covid19test.heathmitchell.repl.co\n"},
        {"exit", "Do you really want to exit [Y/n]? "},
        {"question", "Note: I can't give you 1000% accuracy if you will choose wrong answer!\n\
Do you have coronavirus [Y/n]? "},
        {"negative", "I'm glad to inform you that you don't have coronavirus.\n\
Stay safe!\n"},
        {"positive", "I'm sorry to inform you that you unfortunately have coronavirus.\n\
Contact your local hospital.\n"},
        {"change language", "Choose new language:\n"},
        {"invalid language", "Invalid language!"}
    }},
    {"pl", {
        {"menu", "coronatest v1.0 by Grzesiek11\n\
1000% pewny test na COVID19.\n\
[1] Kontynuuj\n\
[2] O programie\n\
[3] Język\n\
[0] Wyjdź\n"},
        {"about", "1000% pewny, minimalistyczny i otwartoźródłowy test na koronawirusa\n\
GitLab: https://gitlab.com/grzesiek11/coronatest \n\
Mocno inspirowane https://covid19test.heathmitchell.repl.co\n"},
        {"exit", "Czy na pewno chcesz wyjść [Y/n]? "},
        {"question", "Uwaga: Nie gwarantuję 1000% pewności jeśli odpowiesz błędnie!\n\
Czy masz koronawirusa [Y/n]? "},
        {"negative", "Na szczęście nie masz koronawirusa.\n\
Bądź bezpieczny!\n"},
        {"positive", "Niestety, masz koronawirusa.\n\
Skontaktuj się ze swoim lokalnym szpitalem.\n"},
        {"change language", "Wybierz nowy język:\n"},
        {"invalid language", "Niepoprawny język!"}
    }}
};

std::string language = "en";

void clear() {
    for (int i = 0; i < 100; ++i) {
        std::cout << '\n';
    }
}

void test() {
    clear();
    std::string yn;
    std::cout << lang[language]["question"];
    std::cin >> yn;
    clear();
    if (yn == "n") {
        std::cout << lang[language]["negative"];
    } else {
        std::cout << lang[language]["positive"];
    }
    std::cin.get();
    std::cin.ignore();
}

int main() {
    while (1) {
        clear();
        int choice;
        std::cout << lang[language]["menu"];
        std::cin >> choice;
        switch (choice) {
            case 1:
                test();
                break;

            case 2:
                clear();
                std::cout << lang[language]["about"];
                std::cin.get();
                std::cin.ignore();
                break;

            case 3:
                {
                    while (1) {
                        clear();
                        std::string choice;
                        std::cout << lang[language]["change language"];
                        std::vector<std::string> avaliableLangs;
                        for (auto l: lang) {
                            avaliableLangs.push_back(l.first);
                        }
                        for (std::string lName: avaliableLangs) {
                            std::cout << lName << '\n';
                        }
                        std::cin >> choice;
                        if (std::find(avaliableLangs.begin(), avaliableLangs.end(), choice) != avaliableLangs.end()) {
                            language = choice;
                            break;
                        } else {
                            std::cout << lang[language]["invalid language"];
                        }
                    }
                }
                break;

            case 0:
                {
                    clear();
                    std::string yn;
                    std::cout << lang[language]["exit"];
                    std::cin >> yn;
                    if (yn == "n") {
                        break;
                    } else {
                        return 0;
                    }
                }
        }
    }
}